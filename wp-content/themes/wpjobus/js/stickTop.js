jQuery(
function($) {

	if(($("#single-company").length) || ($("#single-job").length) || ($("#single-resume").length)) {
	
		$(document).ready(function(){
			var contentButton = [];
			var contentTop = [];
			var content = [];
			var lastScrollTop = 0;
			var scrollDir = '';
			var itemClass = '';
			var itemHover = '';
			var menuSize = null;
			var stickyHeight = 0;
			var stickyMarginB = 0;
			var currentMarginT = 0;
			var topMargin = 0;
			$(window).scroll(function(event){
	   			var st = $(this).scrollTop();
	   			if (st > lastScrollTop){
	       			scrollDir = 'down';
	   			} else {
	      			scrollDir = 'up';
	   			}
	  			lastScrollTop = st;
			});
			$.fn.stickUp = function( options ) {
				// adding a class to users div
                var $menu = $(this);
                $menu.addClass('stuckMenu');
	        	//getting options
	        	var objn = 0;
	        	if(options != null) {
		        	for(var o in options.parts) {
		        		if (options.parts.hasOwnProperty(o)){
		        			content[objn] = options.parts[objn];
		        			objn++;
		        		}
		        	}
		  			if(objn == 0) {
		  				console.log('error:needs arguments');
		  			}

		  			itemClass = options.itemClass;
		  			itemHover = options.itemHover;
		  			if(options.topMargin != null) {
		  				if(options.topMargin == 'auto') {
		  					topMargin = parseInt($('.stuckMenu').css('margin-top'));
		  				} else {
		  					if(isNaN(options.topMargin) && options.topMargin.search("px") > 0){
		  						topMargin = parseInt(options.topMargin.replace("px",""));
		  					} else if(!isNaN(parseInt(options.topMargin))) {
		  						topMargin = parseInt(options.topMargin);
		  					} else {
		  						console.log("incorrect argument, ignored.");
		  						topMargin = 0;
		  					}	
		  				}
		  			} else {
		  				topMargin = 0;
		  			}
		  			menuSize = $('.'+itemClass).size();
	  			}			
				stickyHeight = parseInt($menu.height());
				stickyMarginB = parseInt($menu.css('margin-bottom'));
				currentMarginT = parseInt($menu.next().closest('div').css('margin-top'));
				var bottomUpPanel = $('.upperPanel').height() + $('.upperPanel').position().top,
                    vartop = parseInt($menu.offset().top) - bottomUpPanel;
				//$(this).find('*').removeClass(itemHover);

                var fixHeader = function() {
                    var varscroll = parseInt($(document).scrollTop());
                    if(menuSize != null){
                        for(var i=0;i < menuSize;i++)
                        {
                            contentTop[i] = $('#'+content[i]+'').offset().top;
                            function bottomView(i) {
                                contentView = $('#'+content[i]+'').height()*.4;
                                testView = contentTop[i] - contentView;
                                //console.log(varscroll);
                                if(varscroll > testView){
                                    $menu.find('.'+itemClass).removeClass(itemHover);
                                    $menu.find('.'+itemClass+':eq('+i+')').addClass(itemHover);
                                } else if(varscroll < 50){
                                    $menu.find('.'+itemClass).removeClass(itemHover);
                                    $menu.find('.'+itemClass+':eq(0)').addClass(itemHover);
                                }
                            }
                            if(scrollDir == 'down' && varscroll > contentTop[i]-50 && varscroll < contentTop[i]+50) {
                                $menu.find('.'+itemClass).removeClass(itemHover);
                                $menu.find('.'+itemClass+':eq('+i+')').addClass(itemHover);
                            }
                            if(scrollDir == 'up') {
                                bottomView(i);
                            }
                        }
                    }

                    if(vartop < varscroll + topMargin){
                        $menu.addClass('isStuck');
                        $('#single-resume').addClass('pdStuck');
                        $('#single-company').addClass('pdStuck');
                        $menu.next().closest('div').css({
                            'margin-top': stickyHeight + stickyMarginB + currentMarginT + 'px'
                        }, 10);
                        $menu.css("position","fixed");
                        $menu.css({
                            top: bottomUpPanel + 'px'
                        });
                    };

                    if(varscroll + topMargin < vartop){
                        $menu.removeClass('isStuck');
                        $('#single-resume').removeClass('pdStuck');
                        $('#single-company').removeClass('pdStuck');
                        $menu.next().closest('div').css({
                            'margin-top': currentMarginT + 'px'
                        }, 10);
                        $menu.css({
                            'position': 'relative',
                            'top': ''
                        });
                    };
                };
                $(document).ready(fixHeader);
                $(window).bind('resize', fixHeader);
                $(window).on('scroll', fixHeader);
			}
		});
	
	}

});