<?php
/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); ?>

    <?php
         include (TEMPLATEPATH . "/part-sliders.php");
    ?>

	<section id="seacrh-result-title">

		 <div class="container">

        	<h2>Страница не найдена</h2>

        </div>

	</section>

    <section id="ads-homepage">
        
        <div class="container">

        	<h2>Возможно вы неправильно ввели/скопировали адрес ссылки. Попробуйте начать свой путь с <a href="<? echo home_url()?>">главной страници каталога</a></h2>

        </div>

    </section>

<?php get_footer(); ?>