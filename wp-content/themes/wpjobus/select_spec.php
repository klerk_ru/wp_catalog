<select name="<? echo empty($params['name']) ? 'job_career_level' : $params['name']; ?>[]" class="select2-spec" multiple="multiple" style="width: <? echo empty($params['width']) ? '100%' : $params['width']; ?>;" size="1">
<?
    global $career_spec;
    foreach ($career_spec as $group) {
        ?>
        <optgroup label="<?=$group['title']?>">
            <?
            foreach ($group['children'] as $id=>$title) {
                $id = ''.$id;
                ?><option value="<?=$id?>"<? if ((is_array($selected) && in_array($id, $selected)) || (!is_array($selected) && $id == $selected)) echo ' selected="selected"' ?>><?=$title?></option><?
            }
            ?>
        </optgroup>
        <?
    }
?>
</select>
<script language="JavaScript">
    (function($) {
        $(function() {
            $(".select2-spec").select2({
                language: 'ru',
                width: 'resolve',
                multiple: true,
                allowClear: true,
                placeholder: {
                    id: "0",
                    text: "<? echo empty($params['placeholder']) ? "Все области" : $params['placeholder'] ?>"
                }
            });
        })
    })(jQuery)
</script>