<?php

class KlerkAPI {

    static private $instance;
    static public function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private $config = array(
        'host' => KLERK_HOST,
    );

    private function __construct() {  }

    private function log($msg) {
        /*
        $fh = fopen(Configure::read('log_path').'/landing_'.$landing.'.log', 'a+');
        fwrite($fh, "[".date('d.m.Y H:i:s')."] IP: ".$client_ip."; user_id: ".$user_id."\n".$msg."\n");
        fclose($fh);*/
    }

    private function request($method) {
        $result = false;
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, 'http://'.$this->config['host'].'/api/'.$method.'/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, false);

            if (!empty($_COOKIE)) {
                $cookie_str = '';
                foreach ($_COOKIE as $name=>$value) {
                    $cookie_str .= $name.'='.$value.';';
                }
                curl_setopt($curl, CURLOPT_COOKIE, rtrim($cookie_str, ';'));
            }

            $result = curl_exec($curl);
            if (!empty($result)) {
                $result = iconv('cp1251', 'utf-8', $result);
            }

            if (curl_errno($curl) != '') {
                $this->log('Fail POST CURL: '.curl_errno($curl).', '.curl_error($curl));
            } else {
                $this->log('Success POST CURL: '.var_export($result, true));
            }
            curl_close($curl);
        } else {
            $this->log('Fail init CURL');
        }

        return $result;
    }

    public function header() {
        return $this->request('header_wp');
    }

    public function footer() {
        return $this->request('footer_wp');
    }
}