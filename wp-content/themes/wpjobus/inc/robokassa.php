<?php

class KlerkRobokassa {

    public static $config = array(
        'password1'	=> 'Eichak8Dah3ia2j',
        'password2'	=> 'pu9Taesh3eith3o'
    );

    public static function fail() {

    }

    public static function success() {

        $out_summ = intval($_POST['OutSum']);
        $inv_id = $_POST['InvId'];
        $post_id = intval($_POST['shp_id']);
        $request_hash = strtoupper($_POST['SignatureValue']);

        $params_aditional = array(
            'shp_type' => $_POST['shp_type'],
            'shp_service' => $_POST['shp_service'],
            'shp_id' => $_POST['shp_id'],
        );
        ksort($params_aditional, SORT_STRING);

        $hash = $_POST['OutSum'].":$inv_id:".self::$config['password1'];
        foreach ($params_aditional as $name=>$value) {
            $hash .= ':'.$name.'='.$value;
        }
        $hash = strtoupper(md5($hash));

        if ($hash==$request_hash) {
            var_dump('1');
            return false;
        }

        if (empty($post_id)) {
            var_dump('2');
            return false;
        }

        if ($_POST['shp_service'] == 'featured') {
            global $redux_demo;
            $days = $redux_demo[$_POST['shp_type'].'-featured-validity'];
            $currentDate = current_time('timestamp');
            $timestamp = strtotime('+'.$days.' days', $currentDate);
            update_post_meta($post_id, 'wpjobus_featured_post_status', 'featured');

            update_post_meta($post_id, 'wpjobus_featured_activation_date', $currentDate);
            update_post_meta($post_id, 'wpjobus_featured_expiration_date', $timestamp);
            update_post_meta($post_id, 'wpjobus_featured_active_time', $days);
        } else {
            update_post_meta($post_id, 'wpjobus_featured_post_status', 'regular');
        }

        $my_post = array(
            'ID' => $post_id,
            'post_status' => 'publish'
        );

        wp_update_post($my_post);

        wpjobusSendNotifications($post_id);
    }

    public static function result() {

    }

    public static function button($type, $id, $service, $summa, $desc) {

        $params = array(
            'MerchantLogin'=>'Klerk.ru',
            'OutSum'=>$summa,
            'InvId'=>'0',
            'InvDesc'=>$desc
        );

        $params_aditional = array(
            'shp_type' => $type,
            'shp_id' => $id,
            'shp_service' => $service,
        );

        ksort($params_aditional, SORT_STRING);

        $params_all = array_merge($params, $params_aditional);
        $params_str = '';
        foreach ($params_aditional as $name=>$value) {
            $params_str .= ':'.$name.'='.$value;
        }

        $hash = md5($params['MerchantLogin'].":".$params['OutSum'].":".$params['InvId'].":".KlerkRobokassa::$config['password1'].$params_str);

        $params_all['SignatureValue'] = strtoupper($hash);

        return "<script language=JavaScript src='https://auth.robokassa.ru/Merchant/PaymentForm/FormS.js?".http_build_query($params_all)."'></script>";
    }
}

add_action( 'wp_ajax_klerkRobokassaSuccess', 'KlerkRobokassa::success' );
add_action( 'wp_ajax_nopriv_klerkRobokassaSuccess', 'KlerkRobokassa::success');

add_action( 'wp_ajax_klerkRobokassaFail', 'KlerkRobokassa::fail' );
add_action( 'wp_ajax_nopriv_klerkRobokassaFail', 'KlerkRobokassa::fail');

add_action( 'wp_ajax_klerkRobokassaResult', 'KlerkRobokassa::result' );
add_action( 'wp_ajax_nopriv_klerkRobokassaResult', 'KlerkRobokassa::result');
