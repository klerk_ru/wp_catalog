<?php

class klerkdb extends wpdb {
    //public function __construct( $dbuser, $dbpassword, $dbname, $dbhost ) {
    public function __construct($dbuser, $dbpassword, $dbname, $dbhost, $dbcharset) {
        $this->charset = $dbcharset;
        return parent::__construct($dbuser, $dbpassword, $dbname, $dbhost);
    }

    static private $instances = array();

    /**
     * @param string $name
     *
     * @return klerkdb
     */
    static public function get_instance($name='site') {
        $name = in_array($name, array('site', 'forum')) ? $name : 'site';
        if (empty(self::$instances[$name])) {
            if ($name == 'forum') {
                self::$instances[$name] = new self(DB_FORUM_USER, DB_FORUM_PASSWORD, DB_FORUM_NAME, DB_FORUM_HOST, DB_FORUM_CHARSET);
            } else {
                self::$instances[$name] = new self(DB_KLERK_USER, DB_KLERK_PASSWORD, DB_KLERK_NAME, DB_KLERK_HOST, DB_KLERK_CHARSET);
            }
        }

        return self::$instances[$name];
    }
}

class KlerkAuth {
    public function __construct() {

        add_action('login_form_login', array($this, 'klerk_form_login'));
        add_action('init', array($this, 'auth'));
    }

    private function log($msg) {
        $fr = fopen(ABSPATH.'/logs/klerk_auth.log', 'a+');
        fwrite($fr, "[".date('d.m.Y H:i:s')."] ".$msg."\n");
        fclose($fr);
    }

    private function get_user_site($forum_user_id) {
        $klerkdb = klerkdb::get_instance('site');
        $forumdb = klerkdb::get_instance('forum');

        $sql = "SELECT * FROM `user_site` WHERE `forum_id`=".$forum_user_id;
        $site_row = $klerkdb->get_row($sql, ARRAY_A);

        $sql = "SELECT * FROM `user` WHERE `userid`=".$forum_user_id;
        $forum_row = $forumdb->get_row($sql, ARRAY_A);

        if (empty($site_row) && empty($forum_row)) {
            return false;
        }

        if (empty($site_row)) {
            $site_row = array();
        }

        if (empty($forum_row)) {
            $forum_row = array();
        }

        $result = array(
            'user_site' => $site_row,
            'user_forum' => $forum_row,
        );

        $result['email'] = empty($forum_row['email']) ? $site_row['email'] : $forum_row['email'];
        $result['login'] = empty($forum_row['username']) ? $site_row['forum_name'] : $forum_row['username'];
        $result['login_lat'] = ctl_sanitize_title($result['login']);
        $result['cookie_pass'] = md5($forum_row['password'].'L6684d79');
        $result['roleset'] = explode(',', $site_row['roleset']);

        return $result;
    }

    public function auth() {
        $table_rel = 'klerk_users_rel';
        if ( !is_user_logged_in() ) {
            if (!empty($_COOKIE['bbuserid'])) {
                /**
                 * @var $wpdb wpdb
                 */
                global $wpdb;

                $forum_user_id = (int) $_COOKIE['bbuserid'];
                //$forum_user_id
                $sql = "SELECT r.wp_user_id
                    FROM  `".$table_rel."` r
                    WHERE r.forum_id = ".$forum_user_id;
                $user_id = $wpdb->get_var($sql);

                $user_row = $this->get_user_site($forum_user_id);
                if (empty($_COOKIE['bbpassword']) || $user_row['cookie_pass'] != $_COOKIE['bbpassword']) {
                    return true;
                }

                if (empty($user_id)) {
                    $user_data = array(
                        'ID' => '',
                        'user_pass' => wp_generate_password(),
                        'user_login' => $user_row['login_lat'],
                        'user_nicename' => $user_row['login_lat'],
                        'user_url' => $user_row['user_forum']['homepage'],
                        'user_email' => $user_row['email'],
                        'display_name' => $user_row['login'],
                        'nickname' => $user_row['login_lat'],
                        'first_name' => $user_row['user_site']['name'],
                        'user_registered' => date('Y-m-d H:i:s'),
                        'role' => in_array('admin', $user_row['roleset']) ? 'administrator' : get_option('default_role') // Use default role or another role, e.g. 'editor'
                    );

                    $user_id = wp_insert_user( $user_data );
                    if ($user_id instanceof WP_Error) {
                        $this->log('FAIL insert user '.var_export($user_id, true));
                    }

                    $sql = "INSERT INTO `".$table_rel."` SET `forum_id`=".$forum_user_id.", `wp_user_id`=".$user_id;
                    if (!$wpdb->query($sql)) {
                        $this->log('FAIL SQL request "'.$wpdb->last_error.'" SQL:'.$sql);
                    }
                }

                $user_wp = get_user_by('id', $user_id);
                if (!empty($user_wp)) {
                    wp_set_current_user($user_id, $user_wp->data->user_login);
                    wp_set_auth_cookie($user_id);
                    do_action('wp_login', $user_wp->data->user_login);
                } else {
                    $this->log('Empty user relation FORUM_USER_ID:'.var_export($forum_user_id, true).' WP_user_id:'.var_export($user_id, true));
                }
            }
        } elseif (empty($_COOKIE['bbuserid'])) {
            wp_logout();
            header("Location: ".$_SERVER['REQUEST_URI'], true, 302);
            exit();
        }
    }

    public function klerk_form_login() {
        wp_redirect('/users/login/?target_auth='.urlencode(isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '/hr/'));
    }
}

new KlerkAuth();

function klerk_header() {
    $html = KlerkAPI::getInstance()->header();

    ob_start();
    include(TEMPLATEPATH.'/header-default.php');
    $header_html = "\n".ob_get_contents()."\n";
    ob_end_clean();

    return preg_replace('/<\/head>/', $header_html.'</head>', $html);
}

function klerk_head() {
    // ������� ������ � ���������� ��������� �����,
    // ����� �� ��������� ������ ����� ���� ����� �����������
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_filter( 'wp_head', 'klerk_head', 0);

function klerk_get_city_name($city_id) {
    $klerkdb = klerkdb::get_instance('site');
    $sql = "SELECT `name` FROM `geo_city` WHERE `geo_city_id`=".intval($city_id);
    return $klerkdb->get_var($sql);
}

function klerk_enqueue_scripts () {
    // ��� ��� ��� ���� ����� �� ������� ������ ��������� ������� ������ � ������ � ������
    wp_deregister_script('jquery');
    wp_register_script('jquery', "/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1", false, '1.11.2');
}

add_action( 'wp_enqueue_scripts', 'klerk_enqueue_scripts', 60);